$(function(){
    $(".menu-btn").click(function(){
        $(".dropdown").toggleClass('active')
    })
    $(".menu-closeBtn").click(function(){
        $(".dropdown").removeClass('active')
    })
})
$(function(){
    $(".lists ul li").click(function(){
        $(".lists ul li").not(this).find("p").slideUp();
        $(this).find("p").slideDown();
        // $(".dropdown-body").slideUp();
        $(".lists ul li").removeClass("active")
        $(this).toggleClass("active")
    })
    
    $(".playIcon").on('click', function(){
        $(this).hide();
        $(this).parent().find(".poster").hide();
        $(this).parent().find(".pauseIcon").show();
        let video = $(this).parent().find("video")[0]; 
        video.play();
    });


    $(".pauseIcon").on('click', function(){
        $(this).hide();
        $(this).parent().find(".poster").show();
        $(this).parent().find(".playIcon").show();
        let video = $(this).parent().find("video")[0]; 
        video.pause();
    });
})

